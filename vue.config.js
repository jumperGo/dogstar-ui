module.exports = {
  assetsDir: 'static',
  runtimeCompiler: true,

  css: {
    extract: false
  },

  publicPath: '/dogstar-ui/'
}